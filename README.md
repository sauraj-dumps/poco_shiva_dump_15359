## shiva-user 11 RP1A.200720.011 V12.5.3.0.RJRINXM release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: shiva
- Brand: POCO
- Flavor: shiva-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: V12.5.3.0.RJRINXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: POCO/shiva/shiva:11/RP1A.200720.011/V12.5.3.0.RJRINXM:user/release-keys
- OTA version: 
- Branch: shiva-user-11-RP1A.200720.011-V12.5.3.0.RJRINXM-release-keys
- Repo: poco_shiva_dump_15359


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
